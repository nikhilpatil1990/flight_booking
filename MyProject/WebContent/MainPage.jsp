<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Errorhandler.jsp"%>
  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"/>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="tcal.css" />
	<script type="text/javascript" src="tcal.js"></script> 
<style type="text/css">
<!--
.style2 { 
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
}
.style3 {
	color: #FFFFFF;
	font-weight: bold;
}
.style5 {color: #FFFFFF}
-->
</style>
<style type="text/css">
#tabContainer {
  padding: 25px 15px 0 15px;
  background:#660000;
  
}

ul#topTab {
  list-style-type: none;
  width: 100%;
  position: relative;
  height: 27px;
  font-size: 13px;
  font-weight: bold;
  margin: 0;
  padding: 11px 0 0 0;
}

ul#topTab li {
  display: block;
  float: left;
  margin: 0 0 0 4px;
  height: 27px;
}

ul#topTab li.left {
  margin: 0;
}

ul#topTab li a {
  display: block;
  float: left;
  color: #fff;
  background: #4A6867;
  line-height: 27px;
  text-decoration: none;
  padding: 0 17px 0 18px;
  height: 27px;
}

ul#topTab li a.right {
  padding-right: 19px;
}

ul#topTab li a:hover {
  background: #2E4560;
}

ul#topTab li a.current {
  color: #2E4560;
  background: #fff;
}

ul#topTab li a.current:hover {
  color: #2E4560;
  background: #fff;
}
.style6 {
	font-family: "Courier New", Courier, monospace;
	color: #FF0000;
}
</style>
<script>
$(function(){
	$('#jtSingleTrip').click(function() {
	 if ($('input:radio[name=Journey_type]:checked').val() == 'one_way') {
      //$('input[name=arrivaldate]').attr('disabled',false);
     
      $('#dateArr').attr('disabled',true);
    };
   });
});	
$(document).ready(function(){
    $('#dateArr').attr('disabled',true);
	$('#jtRoundTrip').click(function() {
        if ($('input:radio[name=Journey_type]:checked').val() == 'two_way') {
          $('#dateArr').attr('disabled',false);
        };
         });
});
</script>
<script>
function starttime()
{
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML = h+":"+m+":"+s;
  var time = document.getElementById('time').innerHTML;
 if ( h <12)
	 {
	 	var message = document.getElementById('time');
	 	message.style.color = "blue";
	 	message.innerHTML=" Good Morning.... ".concat(" The time is " + time);
		
	 }
 else
	 {
	    var message = document.getElementById('time');
	 	message.style.color = "blue";
	 	message.innerHTML=" Good Evening.... ".concat(" The time is " + time);
	 }
  
  var t = setTimeout(function(){starttime()},10);
  function checkTime(i) {
	    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}
}
</script>
</head>

<body background="main.jpg" onload="starttime()">
<p id="time"></p>
 <style>
div.transbox
{
  width: 60px;
  height: 100px;
  margin: 30px 50px;
  background-color: #ffffff;
  border: 0.1px solid black;
  opacity:0.8;
  filter:alpha(opacity=60); /* For IE8 and earlier */
}</style>
<form id="form1" action="FlightSearch.jsp" method="post">
  <p>
    <label></label>
  </p>
	
  
  </table>
  <h1 align="center"><em class="tcalOtherMonth style6"><strong>AN Tours </strong></em><br />
  </h1>
  <p>
    <label></label>
    <br /> 
  </p>
  <p>&nbsp;</p>
  <div class="current" id="tabContainer">
     <% if(session.getAttribute("name")!=null) {%>
    <label align="right">
<%=session.getAttribute("name")%>
 <input name="Submit3" type="submit" class="style6" value="Logout" onclick="this.form.action='Logout.jsp'" />
<%} 
     else {
     %>
</label>
<input name="Submit2" type="submit" class="style6" value="Login/Register" onclick="this.form.action='Login.jsp'" />
   <%} %>
    <ul id="topTab">
      <li><a href="" title="Home" class="current">Home</a></li>
      <li><a href="" title="Home">Online Check-in</a></li>
      <li><a href="About-us.jsp" title="Home">About Us</a></li>
      <li><a href="" title="Home">Contact Us</a></li>
    </ul>
  </div>
   <div class="transbox">
  <table width="700" border="0.5" bgcolor="#006666">
    <tr>
      <td 	width="100" bgcolor="#CC6600"><label>
        <input type="radio" name="Journey_type" id="jtSingleTrip" value="one_way" checked />
      <span class="style2"> One way</span></label></td>
      <td width="479" bgcolor="#CC6600"><label>
        <span class="style3">
<input type="radio" name="Journey_type" id="jtRoundTrip" value="two_way" />
<span class="style2"> Round trip</span></span></label></td>
    </tr>
	</table>
	<table width="700" border="0.2" bgcolor="#006666">
    <tr>
      <td height="41" bgcolor="#CC6600"><span class="style5"> Source</span>
              <select name="select1">
          <option>Berlin</option>
          <option>Nuremberg</option>
          <option>Munich</option>
          <option>Dresden</option>
          <option>Stuttgart</option>
          <option>Frankfurt am Mainz</option>
          <option>Dusseldorf</option>
          <option>Cologne</option>
          <option>Hamburg</option>
          <option>Hannover</option>
        </select></td>
      <td bgcolor="#CC6600" align="center"><label><span class="style5">Destination
          </span>
          <select name="select2">
          <option>Berlin</option>
          <option>Nuremberg</option>
          <option>Munich</option>
          <option>Dresden</option>
          <option>Stuttgart</option>
          <option>Frankfurt am Mainz</option>
          <option>Dusseldorf</option>
          <option>Cologne</option>
          <option>Hamburg</option>
          <option>Hannover</option>
          </select>
      </label>
        <label></label></td>
    </tr>
    <tr>
      <td height="42" bgcolor="#CC6600"><label><span class="style5">Departure</span>
          <span class="style5">
		 <input type="text" name="departuredate" id="datedep" class="tcal" placeholder="Select Data" />
          </span></label>
       </td>
      <td bgcolor="#CC6600"><label><span class="style5">Arrival
          </span>
           <span class="style5">
  	 	<input type="text" name="arrivaldate" id="dateArr" class="tcal" value="" placeholder="Select Data" />
          </span>
          </label>
  		</td>
    </tr>
  </table>
  

  </div>
  <hr />
  <input name="Submit" type="submit" value="Flight Search"  align="middle"/>
  <label></label>
  <label></label>
  <label></label>
  <p>
  <label></label>
  </p>
  <p>
  <label></label>
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
    