<%-- 
    Document   : SearchResult
    Created on : May 4, 2014, 9:52:33 PM
    Author     : Arindam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.sql.*,java.util.*,java.io.*"%>
<%!Vector<String> sResult;
	Iterator it;
	private String record = null;
	private String alineName = null;
	private String sourceCity = null;
	private String destCity = null;
	private String date = null;
	private String remSeats = null;%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
<style type="text/css">
#tabContainer {
	padding: 25px 15px 0 15px;
	background: #660000;
}

ul#topTab {
	list-style-type: none;
	width: 100%;
	position: relative;
	height: 27px;
	font-size: 13px;
	font-weight: bold;
	margin: 0;
	padding: 11px 0 0 0;
}

ul#topTab li {
	display: block;
	float: left;
	margin: 0 0 0 4px;
	height: 27px;
}

ul#topTab li.left {
	margin: 0;
}

ul#topTab li a {
	display: block;
	float: left;
	color: #fff;
	background: #4A6867;
	line-height: 27px;
	text-decoration: none;
	padding: 0 17px 0 18px;
	height: 27px;
}

ul#topTab li a.right {
	padding-right: 19px;
}

ul#topTab li a:hover {
	background: #2E4560;
}

ul#topTab li a.current {
	color: #2E4560;
	background: #fff;
}

ul#topTab li a.current:hover {
	color: #2E4560;
	background: #fff;
}

.style6 {
	font-family: "Courier New", Courier, monospace;
	color: #FF0000;
}
</style>

</head>
<body>
	<h1 align="center">
		<em class="tcalOtherMonth style6"><strong>AN Tours </strong></em><br />
	</h1>
	<p>
		<label></label> <br />
	</p>
	<p>&nbsp;</p>

	<div id="tabContainer">
		<ul id="topTab">
			<li><a href="MainPage.jsp" class="current" title="Home">Search</a></li>
			<li><a href="" title="Home">Online Check-in</a></li>
			<li><a href="" title="Home">About Us</a></li>
			<li><a href="" title="Home">Contact Us</a></li>
		</ul>
	</div>
	<form action="BookFlight.jsp" method="post">
		<table width="1000" border="1.0">
			<th>AirLine Name</th>
			<th>Source City</th>
			<th>Destination City</th>
			<th>Departure</th>
			<th>Rem Seat</th>
			<%
				sResult = (Vector<String>) session.getAttribute("result");
				for (int i = 0; i < sResult.size(); i++) {
					String resultSet = sResult.get(i);
					String[] resultArray = resultSet.split("\\|");
					alineName = resultArray[0];
					sourceCity = resultArray[1];
					destCity = resultArray[2];
					date = resultArray[3];
					remSeats = resultArray[4];
			%>

			<tr border="0.8">
				<td align="center"><%=alineName%></td>
				<td align="center"><%=sourceCity%></td>
				<td align="center"><%=destCity%></td>
				<td align="center"><%=date%></td>
				<td align="center"><%=remSeats%></td>
				<td><input type="submit" name="Book" value="Book" /></td>
			</tr>

			<%
				}
			%>
		</table>
		<hr />
	</form>
</body>
</html>
