<%-- 
    Document   : FlightSearch
    Created on : May 4, 2014, 6:05:28 PM
    Author     : Arindam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.sql.*,java.util.*,java.io.*" %>
<%!
    Connection con = null;
    PreparedStatement ps= null;
    ResultSet rs = null;
    Vector<String> result = null;
    String driverName = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://localhost:3306/MyProject";
    String user = "root";
    String pass = "root";
    String sql_select = "select * from flight where source_city = ? and destination_city = ? and date_o = ? and remaining_seats>0 ";
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <%
                 result = new Vector<String>();
                 session.setAttribute("result", result);
    String sou_city = request.getParameter("select1");
    String des_city = request.getParameter("select2");
    String depdate = request.getParameter("departuredate");
    Class.forName(driverName);
    con=DriverManager.getConnection(url,user,pass);
    ps = con.prepareStatement(sql_select);
    ps.setString(1, sou_city);
    ps.setString(2, des_city);
    ps.setString(3, depdate);
    rs = ps.executeQuery();
    
     while ( rs.next() ) {
       String aline  = rs.getString("airline_name");
       String s_city = rs.getString("source_city");
       String d_city = rs.getString("destination_city");
       String date_o = rs.getString("date_o");
       String seats_left =(rs.getString("remaining_seats"));
       String resultset = (aline + " | " + s_city + " | " + d_city + " | " + date_o + " | " + seats_left); 
       result.add(resultset);
     }
      response.sendRedirect("SearchResult.jsp");
   
     %>
    </body>
</html>
