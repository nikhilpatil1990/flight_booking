<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Errorhandler.jsp"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Registration</title>
<script src="registration-validation.js">
</script>
</head>

<body background="nature-wallpapers-hd-8517-8850-hd-wallpapers.jpg" onload="document.registration.FName.focus();">
<h2 style="background-color:#FF0000"><strong><em>AN Flights</em></strong> </h2>

<p><h1 style="color:#333333" style="font-style:italic" align="center" style="font:Arial, Helvetica, sans-serif"><em> Create Your own AN account
 </em></h1></p>
 <style>
div.transbox
{
  width: 400px;
  height: 180px;
  margin: 30px 50px;
  background-color: #ffffff;
  border: 1px solid black;
  opacity:0.6;
  filter:alpha(opacity=60); /* For IE8 and earlier */
}</style>
<form method="post" action="RegistrationData" name="registration" onsubmit="return(formValidation());">
<div class="transbox">
<table   width="450" border="0.5" bgcolor="#CCCCCC">

  <tr>
	<td><label style="font:Arial, Helvetica, sans-serif" > Name</label>
	</td>
  </tr>
  <tr>
	<td><input name="FName" type="text" placeholder="First Name" />
	<td><input name="LName" type="text" placeholder="Last Name" />    
  </tr>
 

  <tr>
  	<td><label>Choose Username</label>
	</td>
  </tr>
  <tr>
    <td><input name="Email" type="text" placeholder="abc@example.com"/>
	</td>
  </tr>
	
  <tr>
  <td><label>Choose Password</label>
  </td>
  </tr>
  <tr>
  <td><input name="Password" type="password" placeholder="Password" />	
  </td>
  </tr>
  
  <tr>
  <td><label>Re-enter Password</label>
  </td>
  </tr>
  <tr>
  <td><input name="RePassword" type="password" placeholder="Password" />	
  </td>
  </tr>
  
  <tr>
  <td><label>Birthday</label>
  </td>
  </tr>
  <tr >
  <td colspan="0">	
  <select name="dob_dd" >
      <option>- DD -</option>
	  <option value="1">1</option>
	  <option value="2">2</option>
	  <option value="3">3</option>
	  <option value="4">4</option>
	  <option value="5">5</option>
	  <option value="6">6</option>
	  <option value="7">7</option>
	  <option value="8">8</option>
	  <option value="9">9</option>
	  <option value="10">10</option>
	  <option value="11">11</option>
	  <option value="12">12</option>
	  <option value="13">13</option>
	  <option value="14">14</option>
	  <option value="15">15</option>
	  <option value="16">16</option>
	  <option value="17">17</option>
	  <option value="18">18</option>
	  <option value="19">19</option>
	  <option value="20">20</option>
	  <option value="21">21</option>
	  <option value="22">22</option>
	  <option value="23">23</option>
	  <option value="24">24</option>
	  <option value="25">25</option>
	  <option value="26">26</option>
	  <option value="27">27</option>
	  <option value="28">28</option>
	  <option value="29">29</option>
	  <option value="30">30</option>
	  <option value="31">31</option>
    </select>
	</td>
   <td>
   <select name="DateOfBirth_Month">
        <option>- Month -</option>
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
    </select> 
  </td>
  <td>
   <select name="DateOfBirth_Year">
    <option>- Year -</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
    <option value="2009">2009</option>
    <option value="2008">2008</option>
    <option value="2007">2007</option>
    <option value="2006">2006</option>
    <option value="2005">2005</option>
    <option value="2004">2004</option>
    <option value="2003">2003</option>
    <option value="2002">2002</option>
    <option value="2001">2001</option>
    <option value="2000">2000</option>
    <option value="1999">1999</option>
    <option value="1998">1998</option>
    <option value="1997">1997</option>
    <option value="1996">1996</option>
    <option value="1995">1995</option>
    <option value="1994">1994</option>
    <option value="1993">1993</option>
    <option value="1992">1992</option>
    <option value="1991">1991</option>
    <option value="1990">1990</option>
    <option value="1989">1989</option>
    <option value="1988">1988</option>
    <option value="1987">1987</option>
    <option value="1986">1986</option>
    <option value="1985">1985</option>
    <option value="1984">1984</option>
    <option value="1983">1983</option>
    <option value="1982">1982</option>
    <option value="1981">1981</option>
    <option value="1980">1980</option>
    <option value="1979">1979</option>
    <option value="1978">1978</option>
    <option value="1977">1977</option>
    <option value="1976">1976</option>
    <option value="1975">1975</option>
    <option value="1974">1974</option>
    <option value="1973">1973</option>
    <option value="1972">1972</option>
    <option value="1971">1971</option>
    <option value="1970">1970</option>
    <option value="1969">1969</option>
    <option value="1968">1968</option>
    <option value="1967">1967</option>
    <option value="1966">1966</option>
    <option value="1965">1965</option>
    <option value="1964">1964</option>
    <option value="1963">1963</option>
    <option value="1962">1962</option>
    <option value="1961">1961</option>
    <option value="1960">1960</option>
    <option value="1959">1959</option>
    <option value="1958">1958</option>
    <option value="1957">1957</option>
    <option value="1956">1956</option>
    <option value="1955">1955</option>
    <option value="1954">1954</option>
    <option value="1953">1953</option>
    <option value="1952">1952</option>
    <option value="1951">1951</option>
    <option value="1950">1950</option>
    <option value="1949">1949</option>
    <option value="1948">1948</option>
    <option value="1947">1947</option>
    <option value="1946">1946</option>
    <option value="1945">1945</option>
    <option value="1944">1944</option>
    <option value="1943">1943</option>
    <option value="1942">1942</option>
    <option value="1941">1941</option>
    <option value="1940">1940</option>
    <option value="1939">1939</option>
    <option value="1938">1938</option>
    <option value="1937">1937</option>
    <option value="1936">1936</option>
    <option value="1935">1935</option>
    <option value="1934">1934</option>
    <option value="1933">1933</option>
    <option value="1932">1932</option>
</select> 
</td>
</tr>
<tr >
<td>
<label>Gender</label>
</td>
</tr>
<tr>
<td>
<select name="Gender">
	<option value="Male">Male</option>
	<option value="Female">Female</option>
</select>
</td>
</tr>

<tr>
<td>
<label>Mobile Phone</label>
</td>
</tr>


<tr>
<td>
<input name="Mobile_No" type="text" placeholder="+49"/>
</td>
</tr>

<tr>
<td>
<label>Address</label>
</td>
</tr>
<tr>
<td>
<input name="Address" type="text" placeholder="Address"/>
</td>
<td>
<input name="Pin" type="text" placeholder="Pincode"/>
</td>

</tr>

 
<tr>
<td>
<label>Country</label>
</td>
</tr>
<tr>
<td>
<input name="Country" type="text" placeholder="Germany"/>
</td>
</tr>
</table>
<table bgcolor="#CCCCCC" width="450" border="0.5">
<tr>
<td>

<input name="Agree" type="checkbox" id="Agree" />
<label style="font:'Courier New', Courier, monospace">I agree to AN terms and Policy</label>
</td>
</tr>    
<tr>
<td>
<input style="background-color:#0099FF"  name="Submit" type="submit" value="Submit"/>
 </td>
 </tr>
 </table>
 </div>
</form>
</body>
</html>
    