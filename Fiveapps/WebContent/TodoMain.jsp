<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%!
	ArrayList<String> todo = null;
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>List</title>
</head>
<body style="font-style:italic">
<%
	todo = (ArrayList)session.getAttribute("data");
	if (session.isNew())
	{
		todo = new ArrayList<String>();
		session.setAttribute("data", todo);
	}
%>
<form method="post" action="AddTodo.jsp" >
<ol>
<%
	for ( int i = 0; i < todo.size(); i ++)
	{
		out.println("<li>"+todo.get(i) );	
	}
%>
</ol>

Add: <input type="text" name="add" placeholder="add todo"/></br>
<input name="Add" type="submit" />
</form>
</body>
</html>