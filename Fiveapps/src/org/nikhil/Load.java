package org.nikhil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Load
 */
@WebServlet(name = "Map", urlPatterns = { "/Map" })
public class Load extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Load() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ServletConfig config = getServletConfig();
		HttpSession session = request.getSession();
		String str= config.getServletContext().getRealPath("/") + "translate.txt";
		
		
		ReadFile file = new ReadFile(str);
		HashMap<String,String> mapper =file.getMapper();
		String check = request.getParameter("input");
		for (String key: mapper.keySet())
		{
			if (key.equals(check))
			{
				session.setAttribute("Key", mapper.get(key));
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("TranslateMain.jsp");
	            dispatcher.forward(request, response);     
			}
		}
		}
		


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.doGet(request, response);
	}
}

